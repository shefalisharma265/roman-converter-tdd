package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

    public RomanConverter() {
    }
    private int letterToNumber(char letter) {
        switch (letter) {
            case 'I':  return 1;
            case 'V':  return 5;
            case 'X':  return 10;
            case 'C':  return 100;
            case 'M':  return 1000;
            default:   throw new NumberFormatException(
                    "Illegal character");
        }
    }
    public Integer convertRomanToArabicNumber(String roman) {
        int i=0;
        int decimal=0;
        if(roman.length()==0)
            throw new NumberFormatException("Empty String");

        while(i<roman.length()){
            char letter= roman.charAt(i);
           int number=letterToNumber(letter);
           i++;
           if(i==roman.length()){
              decimal+=number;
           }
           else{
               int nextNumber= letterToNumber(roman.charAt(i));
               if (nextNumber > number) {
                   decimal += (nextNumber - number);
                   i++;
               }
               else {
                   decimal += number;
               }
           }
        } return decimal;
    }
}

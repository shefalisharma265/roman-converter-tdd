package com.thoughtworks.vapasi;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {
    private Rectangle rectangle;
    private static Stream<Arguments> provideLengthAndBreadthAndExpectedArea() {
        return Stream.of(
                Arguments.of(3,4,12),
                Arguments.of(4,5,20)
        );
    }
    private static Stream<Arguments> provideLengthAndBreadthAndExpectedPerimeter() {
        return Stream.of(
                Arguments.of(3,4,14),
                Arguments.of(4,5,18)
        );
    }


    @ParameterizedTest
    @MethodSource({"provideLengthAndBreadthAndExpectedArea"})
    void shouldCalculateArea(float length,float breadth,float expectedArea){
            rectangle=new Rectangle(length,breadth);
        float actualValue=rectangle.calculateArea();
        assertEquals(expectedArea,actualValue,"Wrong Area");

    }

    @ParameterizedTest
    @MethodSource({"provideLengthAndBreadthAndExpectedPerimeter"})
    void calculatePerimeter(float length,float breadth,float expectedPerimeter){
        rectangle=new Rectangle(length,breadth);
        float actualValue=rectangle.calculatePerimeter();
        assertEquals(expectedPerimeter,actualValue,"Wrong perimeter");

    }


}

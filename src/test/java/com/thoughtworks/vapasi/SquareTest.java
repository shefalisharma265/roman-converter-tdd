package com.thoughtworks.vapasi;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SquareTest {
    private Square square;
    private static Stream<Arguments> provideInputSideAndExpectedArea() {
        return Stream.of(
                Arguments.of(3,9),
                Arguments.of(4,16)
        );
    }
    private static Stream<Arguments> provideInputSideAndExpectedPerimeter() {
        return Stream.of(
                Arguments.of(5,20),
                Arguments.of(6,24)
        );
    }

    @ParameterizedTest
    @MethodSource({"provideInputSideAndExpectedArea"})
    void shouldCalculateArea(float side,float expectedArea){
        square=new Square(side);
       float actualArea = square.calculateArea();
       assertEquals(expectedArea,actualArea,"Wrong Area");
    }

    @ParameterizedTest
    @MethodSource({"provideInputSideAndExpectedPerimeter"})
    void shouldCalculatePerimeter(float side,float expectedPerimeter){
        square=new Square(side);
       float actualPerimeter = square.calculatePerimeter();
       assertEquals(expectedPerimeter,actualPerimeter,"Wrong Perimeter");
    }
}

package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RomanConverterTest {

    private RomanConverter romanConvertor;


    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    private static Stream<Arguments> provideCombinationOfInputAndExpectedValue() {
        return Stream.of(
                Arguments.of("I", 1),
                Arguments.of("II", 2),
                Arguments.of("III", 3),
                Arguments.of("IV", 4),
                Arguments.of("V", 5),
                Arguments.of("VI", 6),
                Arguments.of("VII", 7),
                Arguments.of("IX", 9),
                Arguments.of("XXXVI", 36),
                Arguments.of("MMXII", 2012),
                Arguments.of("MCMXCVI", 1996)
        );
    }





    @ParameterizedTest
    @MethodSource("provideCombinationOfInputAndExpectedValue")
     void shouldCovertTheRomanNumber(String input,Integer expected) {
        Integer actualValue = romanConvertor.convertRomanToArabicNumber(input);
        assertEquals(expected, actualValue);
    }


    @Test()
     void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {

        String romanNumber="XYZ";
        assertThrows(IllegalArgumentException.class,
                ()->{
                    romanConvertor.convertRomanToArabicNumber(romanNumber);
                });

    }
}